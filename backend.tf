terraform {
  backend "s3" {
    bucket         = "301725824318-eu-west-1-tf-state"
    key            = "project/base/terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "301725824318-eu-west-1-tf-lock"
    encrypt        = true
  }
}

