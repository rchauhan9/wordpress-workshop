output "database_endpoint" {
  value = aws_db_instance.wordpress.endpoint
}

output "website_url" {
  value = "http://${module.alb.this_lb_dns_name}"
}

