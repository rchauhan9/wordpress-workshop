locals {
    vpc_id                    = data.aws_ssm_parameter.vpc_id.value
    default_security_group_id = data.aws_ssm_parameter.vpc_default_sg_id.value
    private_subnets           = split(",", data.aws_ssm_parameter.vpc_private_subnet_ids.value)
    public_subnets            = split(",", data.aws_ssm_parameter.vpc_public_subnet_ids.value)
}

data aws_ssm_parameter "vpc_id" {
    name = "VPCId"
}

data aws_ssm_parameter "vpc_default_sg_id" {
    name = "DefaultSecurityGroupId"
}

data aws_ssm_parameter "vpc_public_subnet_ids" {
    name = "PublicSubnetIds"
}

data aws_ssm_parameter "vpc_private_subnet_ids" {
    name = "PrivateSubnetIds"
}

