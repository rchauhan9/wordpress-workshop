### General
variable "aws_region" {
  description = "(Required) The region the resources will be provisioned to."
  type        = string
}

variable "release" {
  description = "Release name"
  type        = string
  default     = "base"
}

### Database
variable "rds_instance_type" {
  description = "(Required) The instance type of the RDS instance."
  type        = string
  default     = "db.m5.large"
}

variable "rds_db_identifier" {
  description = "(Optional, Forces new resource) The name of the RDS instance, if omitted, Terraform will assign a random, unique identifier."
  type        = string
  default     = "wordpress-db"
}

variable "db_name" {
  description = "Name of the database"
  default     = "wordpressdb"
  type        = string
}

variable "db_username" {
  description = "Username for the database"
  default     = "wpadmin"
  type        = string
}

variable "db_password" {
  description = "DO NOT CHANGE - Password for the master DB user. Note that this may show up in logs, and it will be stored in the state file."
  default     = "380cccf909"
  type        = string
}

variable "wp_admin" {
  description = "Username for Wordpress admin"
  default     = "wpadmin"
  type        = string
}

variable "wp_password" {
  description = "Default administrator password for the Wordpress admin"
  default     = "WpPassword"
  type        = string
}


