#Entrypoint is also needed as image by default set `terraform` binary as an
# entrypoint.
image:
  name: registry.gitlab.com/gitlab-org/gitlab-build-images:terraform
  entrypoint:
    - "/usr/bin/env"
    - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

# Default output file for Terraform plan
variables:
  PLAN_FILE: plan.tfplan
  JSON_PLAN_FILE: tfplan.json
  JSON_PLAN_SUMMARY_FILE: tfplan_summary.json

cache:
  paths:
    - .terraform

before_script:
  - alias convert_report="jq -r '([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"
  - apk add curl
  - terraform --version
  - curl -L "$(curl -Ls https://api.github.com/repos/terraform-linters/tflint/releases/latest | grep -o -E "https://.+?_linux_amd64.zip")" -o tflint.zip && unzip tflint.zip && rm tflint.zip && chmod 0755 tflint && mv tflint /usr/local/bin
  - tflint --version

stages:
  - init
  - validate
  - build
  - deploy

init-job:
  stage: init
  script:
    - terraform init

validate-job:
  stage: validate
  script:
    - terraform fmt
    - terraform validate
    - tflint

plan-job:
    stage: build
    script:
        - terraform plan -out=$PLAN_FILE -var-file=terraform.tfvars
        - echo Producing JSON version of plan file
        - terraform show --json $PLAN_FILE > $JSON_PLAN_FILE
        - terraform show --json $PLAN_FILE | convert_report > $JSON_PLAN_SUMMARY_FILE
    artifacts:
        paths:
            - $PLAN_FILE
            - $JSON_PLAN_FILE
            - $JSON_PLAN_SUMMARY_FILE
        reports:
            terraform: $JSON_PLAN_SUMMARY_FILE

# Separate apply job for manual launching Terraform as it can be a destructive action
apply-job:
    stage: deploy
    environment:
        name: production
    script:
        - terraform apply -input=false $PLAN_FILE
    dependencies:
        - plan-job
    rules:
        - if: '$CI_COMMIT_BRANCH == "master"'
          when: manual

